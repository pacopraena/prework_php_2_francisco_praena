<?php

function fiveVocals($text){
    $allVocals = false;
    $vocals = ['a', 'e', 'i', 'o', 'u'];

    foreach($vocals as $vocal){
        $pos = strpos($text, $vocal);

        if($pos === false){
            $allVocals = false;
            break;
        }else{
            $allVocals = true;
        }

    }

    if($allVocals == true){
        echo "LA PALABRA CONTIENE LAS 5 VOCALES";
    }else{
        echo "NO CONTIENE TODAS LAS VOCALES";
    }
}

?>

<p><?php isset($_POST['texto']) ? fiveVocals($_POST['texto']) :  '' ?></p>